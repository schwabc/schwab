DROP SCHEMA if exists schwab CASCADE; commit;
create schema if not exists schwab  AUTHORIZATION  sa;
set schema schwab;

create sequence if not exists user_seq;

DROP TABLE if exists schwab.schwab_user; commit;
create table if not exists schwab.schwab_user (
    id long primary key not null,
    email varchar(255),
    password varchar(255),
    status varchar(15),
    primary key (id)
);
commit;