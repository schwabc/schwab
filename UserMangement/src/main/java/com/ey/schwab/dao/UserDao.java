package com.ey.schwab.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ey.schwab.bean.User;

@Repository
public interface UserDao extends JpaRepository<User, Long> {

	@Query("from User where email=:email and status='ACTIVE'")
	User findByEmail(String email);

}
