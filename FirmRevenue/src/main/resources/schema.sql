DROP SCHEMA if exists SCHWAB CASCADE; commit;
create schema if not exists SCHWAB  AUTHORIZATION  sa;
set schema SCHWAB;
create sequence if not exists firm_seq;

DROP TABLE if exists SCHWAB.FIRM; commit;
create table if not exists SCHWAB.FIRM (
    id long primary key not null,
    year long not null,
    amount long not null,
    primary key (id)
);
commit;