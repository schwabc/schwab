DROP SCHEMA if exists SCHWAB CASCADE; commit;
create schema if not exists SCHWAB  AUTHORIZATION  sa;
set schema SCHWAB;

create sequence if not exists asset_seq;
--DESCRETIONARY value 1 is DESCRETIONARY and 2 is NON-DESCRETIONARY
DROP TABLE if exists SCHWAB.ASSETS; commit;
create table if not exists SCHWAB.ASSETS (
    id long primary key not null,
    year long not null,
    amount long not null,
    DESCRETIONARY long,
    primary key (id)
);
commit;